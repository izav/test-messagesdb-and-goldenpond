﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.Models.GoldenPond
{
    public enum Orientation { N, E, S, W };
    public enum MarineCommand { P, S, F };

    public class DuckState
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Orientation Orientation { get; set; }

        public const int MaxX = 5;
        public const int MaxY = 5;


        public DuckState() { }

        public DuckState(DuckState state) {
            this.X = state.X;
            this.Y = state.Y;
            this.Orientation = state.Orientation;
        }

      
        public DuckState Move( MarineCommand cmd) {
            int temp;
            switch (cmd)
            {
                case MarineCommand.P:
                    temp = ((int)(this.Orientation + 4) -1) % 4;
                    this.Orientation = (Orientation)temp;
                    break;

                case MarineCommand.S:
                    temp  = ((int)(this.Orientation) + 1) % 4;
                    this.Orientation = (Orientation)temp;
                    break;

                case MarineCommand.F:
                    this.Delta();                 
                    break;
            }

            return this;
        }


        public DuckState Delta()
        {
           switch (this.Orientation)
           {
               case Orientation.N:
                   this.Y = this.Y == MaxY ? MaxY : this.Y + 1;
                   break;
               case Orientation.E:
                   this.X = this.X == MaxX ? MaxX : this.X + 1;
                   break;
               case Orientation.S:
                   this.Y = this.Y == 0 ?  0 : this.Y - 1;
                   break;
               case Orientation.W:
                   this.X = this.X == 0 ? 0 : this.X - 1;
                   break;
           }
            
            return this;
        }


        public DuckState FollowCommands(string stringCommands)
        {
            IEnumerable<MarineCommand> commands = stringCommands
                        .ToCharArray()
                        .Select(ch => (MarineCommand)Enum.Parse(typeof(MarineCommand), ch.ToString()))
                        .ToList();
            foreach (var command in commands)
            {
                this.Move(command);           
            }
            return this;
        }        
    }
}