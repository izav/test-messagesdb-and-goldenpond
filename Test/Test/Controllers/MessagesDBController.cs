﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test.ViewModels;
using Test.Repositories.MessagesDB;
using Test.Models.MessagesDB;

namespace Test.Controllers
{
    public class MessagesDBController : Controller
    {
        private IMessagesDBRepository<Person, int> _repo;

        public MessagesDBController(IMessagesDBRepository<Person, int> repo)
       {
           _repo = repo;
       }

        public ActionResult TestEF()
        {
             var domains = _repo.GetDomains();
             var emailAddressesWithPersonNames = _repo.GetEmailAddressesWithPersonNames();
             var emailsCountForDomains = _repo.GetEmailsCountForDomains();

             var vm = new MessagesDBViewModel()
             {
                 Title = "EF",
                 Domains = domains,
                 EmailAddressesWithPersonNames = emailAddressesWithPersonNames,
                 EmailsCountForDomains = emailsCountForDomains,
             };
            return View("Test",vm);
        }

        public ActionResult TestSQL()
        {
            var domains = _repo.GetDomainsSQL();
            var emailAddressesWithPersonNames = _repo.GetEmailAddressesWithPersonNamesSQL();
            var emailsCountForDomains = _repo.GetEmailsCountForDomainsSQL();

            var vm = new MessagesDBViewModel()
            {
                Title = "SQL",
                Domains = domains,
                EmailAddressesWithPersonNames = emailAddressesWithPersonNames,
                EmailsCountForDomains = emailsCountForDomains,
            };
            return View("Test", vm);
        }
    }
}