﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using System.Configuration;
using System.Web.Routing;
using System.Web.Mvc;
using Test.Repositories.MessagesDB;
using Test.Models.MessagesDB;

namespace Test.Controllers
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel kernel;

        public NinjectControllerFactory()
        {
            kernel = new StandardKernel();
            AddBindingsDB();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)kernel.Get(controllerType);
        }


        private void AddBindingsDB()
        {
            kernel.Bind<IMessagesDBRepository<Person, int>>().To<MessagesDBRepsository>();
        }
    }
}