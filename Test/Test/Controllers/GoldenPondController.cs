﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test.Models.GoldenPond;
using Test.ViewModels;

namespace Test.Controllers
{
    public class GoldenPondController : Controller
    {        
        public ActionResult Test()
        {
            string stringCommands1 = "PFPFPFPFF";                                            
            DuckState stateStart1 = new DuckState() { X = 1, Y = 2, Orientation = Orientation.N };
            DuckState stateEnd1 = new DuckState(stateStart1);
            stateEnd1.FollowCommands(stringCommands1);

            string stringCommands2 = "FFSFFSFSSF";
            DuckState stateStart2 = new DuckState() { X = 3, Y = 3, Orientation = Orientation.E };
            DuckState stateEnd2 = new DuckState(stateStart2);
            stateEnd2.FollowCommands(stringCommands2);

            IEnumerable<GoldenPondViewModel> vm = new List<GoldenPondViewModel>() {

                new GoldenPondViewModel() {                  
                    StartState = stateStart1,
                    EndState = stateEnd1,
                    Commands = stringCommands1
                },
                new GoldenPondViewModel() {
                    StartState = stateStart2,
                    EndState = stateEnd2,
                    Commands = stringCommands2
                },
            };

            return View(vm);
        }
    }
}