﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Test.Models;
using Test.ViewModels;
using Test.Models.MessagesDB;



namespace Test.Repositories
{
 
public class DbContextEFInitializer : DropCreateDatabaseIfModelChanges<DbContextEF>
 {
        protected override void Seed(DbContextEF context)
        {
            InitializeDB(context);           
            base.Seed(context);
        }
     
        private void InitializeDB(DbContextEF context)
        {
            context.Database.ExecuteSqlCommand(
                            @"CREATE PROCEDURE [dbo].[sp_GetDomains]
                            AS
                            BEGIN
                            SET NOCOUNT ON;
                            SELECT * 
                            FROM Domains
                            END;");

            context.Database.ExecuteSqlCommand(
                            @"CREATE PROCEDURE [dbo].[sp_GetEmailAddressesWithPersonNames]
                            AS
                            BEGIN
                            SET NOCOUNT ON;
                            SELECT  e.Id, e.AddressText as Email,  p.Name as Name
                            FROM EmailAddresses e
                            INNER JOIN People p  ON e.PersonId = p.Id
                            ORDER BY e.AddressText, p.Name; 
                            END;");

           context.Database.ExecuteSqlCommand(
                           @"CREATE PROCEDURE [dbo].[sp_GetEmailsCountForDomains]
                           AS
                           BEGIN
                           SET NOCOUNT ON; 
                           DECLARE @fromId int;            
                           SELECT @fromId = Id FROM ParticipantTypes WHERE LOWER(Type) ='from';                     
                           SELECT s.*, d.UrlText
                           FROM (
                                 SELECT e.DomainId, Count(*) as Count 
                                 FROM (SELECT  pp.* from Participants pp WHERE pp.ParticipantTypeId <> @fromId) p
                                 INNER JOIN EmailAddresses e ON  p.EmailAddressId = e.Id 
                                 GROUP BY e.DomainId
                                 ) s
                           INNER JOIN Domains d  ON s.DomainId = d.Id
                           ORDER BY d.UrlText
                           END;");

            var person1 = new Person { Name = "John Doe" };
            var person2 = new Person { Name = "Jannet Smith" };
            var person3 = new Person { Name = "Robert Strong" };
            var person4 = new Person { Name = "Susan Jones" };

            var domain1 = new Domain { UrlText = "gmail.com" };
            var domain2 = new Domain { UrlText = "ValiCorp.com" };
            var domain3 = new Domain { UrlText = "FWork.com" };

            var emailAddress1 = new EmailAddress { AddressText = "John.Doe@gmail.com", Domain = domain1, Person = person1 };
            var emailAddress2 = new EmailAddress { AddressText = "john@ValiCorp.comm", Domain = domain2, Person = person1 };
            var emailAddress3 = new EmailAddress { AddressText = "jsmith@FWork.com", Domain = domain3, Person = person2 };
            var emailAddress4 = new EmailAddress { AddressText = "rstrong@FWork.com", Domain = domain3, Person = person3 };
            var emailAddress5 = new EmailAddress { AddressText = "daisies@gmail.com", Domain = domain1, Person = person3 };
            var emailAddress6 = new EmailAddress { AddressText = "susan@ValiCorp.com", Domain = domain2, Person = person4 };

            var partisipantType1 = new ParticipantType { Type = "From" };
            var partisipantType2 = new ParticipantType { Type = "To" };
            var partisipantType3 = new ParticipantType { Type = "CC" };
            var partisipantType4 = new ParticipantType { Type = "BCC" };

            var message1 = new Message { Subject = " A once in a lifetime chance", MessageText = "To whom it may concern…" };
            var message2 = new Message { Subject = "Demo follow up", MessageText = "Dear sir or madam, … " };             

            var people = new List<Person>() { 
                person1, person2, person3, person4 
            };
            var domains = new List<Domain>() { 
                domain1, domain2, domain3 
            };
            var partisipantTypes = new List<ParticipantType>() { 
                partisipantType1, partisipantType2, partisipantType3, partisipantType4 
            };
            var messages = new List<Message>() { 
                message1, message2 
            };
            var emailAddresses = new List<EmailAddress>() { 
                emailAddress1, emailAddress2, emailAddress3, emailAddress4, emailAddress5, emailAddress6 
            };

            context.People.AddRange(people);
            context.Domains.AddRange(domains);
            context.PartisipantTypes.AddRange(partisipantTypes);
            context.Messages.AddRange(messages);
            context.EmailAddresses.AddRange(emailAddresses);
            context.SaveChanges();

            var participantes = new List<Participant>() { 
                new Participant {   ParticipantType = partisipantType1, EmailAddress = emailAddress1, Message = message1  }, 
                new Participant {   ParticipantType = partisipantType2, EmailAddress = emailAddress2, Message = message1  },
 
                new Participant {   ParticipantType = partisipantType2, EmailAddress = emailAddress3, Message = message1  }, 
                new Participant {   ParticipantType = partisipantType1, EmailAddress = emailAddress2, Message = message2  }, 
               
                new Participant {   ParticipantType = partisipantType2, EmailAddress = emailAddress3, Message = message2  }, 
                new Participant {   ParticipantType = partisipantType3, EmailAddress = emailAddress4, Message = message2  },                               
            };
           
            context.Participantes.AddRange(participantes);
            context.SaveChanges();
        
        }
    }
}