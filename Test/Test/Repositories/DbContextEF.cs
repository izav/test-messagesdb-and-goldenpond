﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Infrastructure;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Test.Models.User;
using Test.Models.MessagesDB;
using Test.Models;


namespace Test.Repositories
{
    public class DbContextEF : IdentityDbContext<ApplicationUser>
    {        
        public DbSet<Person> People { get; set; }
        public DbSet<Domain> Domains { get; set; }
        public DbSet<ParticipantType> PartisipantTypes { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<EmailAddress> EmailAddresses { get; set; }
        public DbSet<Participant> Participantes { get; set; }

        public DbContextEF(): base("DefaultConnection")
        { 
             this.Configuration.LazyLoadingEnabled = true; 
         }

        public static DbContextEF Create()
        {
            return new DbContextEF();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Person>()
                .ToTable("People");

            modelBuilder.Entity<Domain>()
               .Property(e => e.Id)
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<ParticipantType>()
               .Property(e => e.Id)
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Message>()
               .Property(e => e.Id)
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<EmailAddress>()
               .Property(e => e.Id)
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Participant>()
               .Property(e => e.Id)
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }      
    }
}