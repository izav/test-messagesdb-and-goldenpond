﻿# **Messages Database & Golden Pond**

Software Used:

C#, MVC, MS SQL Server, EF(Code First), Bootstrap


--------------------------------------------------
tst20128-001-site1.atempurl.com

## **1) Messages Database**

**EF - [http://tst20128-001-site1.atempurl.com/MessagesDB/TestEF](http://tst20128-001-site1.atempurl.com/MessagesDB/TestEF)**   
**SQL - [http://tst20128-001-site1.atempurl.com/MessagesDB/TestSQL](http://tst20128-001-site1.atempurl.com/MessagesDB/TestSQL)**   

**Test**

![test.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/test.jpg)

Given the above database tables (with some sample data) write a SQL query that

1. lists of each domain in the system 
2. lists of each email address along with the owners real name  
3. lists each domain and the total number of emails sent to it (combining To, CC, & BCC)

---------------------------------------------
**Screenshot**



![T8.png](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/T8.png)

---------------------------------------------

## **2) Golden Pond**    

**[http://izdemo1-001-site1.dtempurl.com/GoldenPond/Test](http://izdemo1-001-site1.dtempurl.com/GoldenPond/Test)** 


**Test**

You are writing a simulation of ducks on a curiously rectangular pond. A ducks position and location is represented by a combination of x and y co-ordinates and a letter representing one of the four cardinal compass points. 
The pond is divided up into a grid to simplify navigation. An example position might be 0, 0, N, which means the duck is in the bottom left corner and facing North.  
In order to control a duck, you send a simple string of letters. The possible letters are 'P', 'S' and 'F'. 'P' and 'S' makes the duck spin 90 degrees toward port side (left) or starboard (right) respectively, without moving from its current spot. 'F' means move forward one grid point, and maintain the same heading.

Assume that the square directly North from (x, y) is (x, y+1).

* Test Input:   
1 2 N  PFPFPFPFF    
3 3 E  FFSFFSFSSF  
* Expected Output:   
1 3 N  
5 1 E  


----------------------------------------------

**Screenshot**


![T6-1.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/T6-1.jpg)




